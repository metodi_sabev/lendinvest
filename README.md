# Lendinvest test task

## Requirements

- PHP >= 7.1

## Installation

1) Install composer dependencies:

```
$ composer install
```

## Run the application

You can run the application by using the following command:

```
$ composer launch
```

The result will be printed out in the same terminal window.

## Run tests

To simply run the Unit tests, use the following command:

```
$ composer test
```

To run the Unit tests and generate a coverage report (which can be found in the tmp folder), use the following command:

```
$ composer test-coverage
```
