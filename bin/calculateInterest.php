<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Lendinvest\Application;

$app = new Application();
$app->start();
