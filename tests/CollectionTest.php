<?php

namespace LendinvestTest;

use Lendinvest\Collection;

class CollectionTest extends \PHPUnit\Framework\TestCase
{
    public function setUp()
    {
        $this->collection = new Collection();
    }

    public function testConstruction()
    {
        $this->assertInstanceOf(Collection::class, $this->collection);
    }

    public function testAddItem()
    {
        $obj = new \stdClass();
        $obj->test = 'testElement';

        $this->collection->add($obj);

        $this->assertTrue($this->collection->keyExists(0));
        $this->assertInstanceOf(\stdClass::class, $this->collection->get(0));
        $this->assertEquals($obj, $this->collection->get(0));
    }

    public function testAddItemWithKey()
    {
        $key = 'myStdObject';

        $obj = new \stdClass();
        $obj->test = 'testElement';

        $this->collection->add($obj, $key);

        $this->assertTrue($this->collection->keyExists($key));
    }

    public function testAddItemWithExistingKey()
    {
        $this->expectException(\Exception::class);

        $key = 'myStdObject';

        $obj = new \stdClass();
        $obj->test = 'testElement';

        $obj2 = new \stdClass();

        $this->collection->add($obj, $key);
        $this->collection->add($obj2, $key);
    }

    public function testDeleteItem()
    {
        $key = 'myStdObject';
        $obj = new \stdClass();

        $this->collection->add($obj, $key);
        $this->collection->delete($key);

        $this->assertEquals($this->collection->length(), 0);
    }

    public function testDeleteItemWithInvalidKey()
    {
        $this->expectException(\Exception::class);

        $key = 'myStdObject';
        $obj = new \stdClass();

        $key2 = 'invalidKey';

        $this->collection->add($obj, $key);
        $this->collection->delete($key2);
    }

    public function testGetItem()
    {
        $key = 'myStdObject';
        $obj = new \stdClass();
        $obj->test = 'testElement';

        $this->collection->add($obj, $key);

        $this->assertInstanceOf(\stdClass::class, $this->collection->get($key));
        $this->assertEquals($obj, $this->collection->get($key));
    }

    public function testGetItemWithInvalidKey()
    {
        $this->expectException(\Exception::class);

        $this->collection->get('invalidKey');
    }

    public function testkeyExists()
    {
        $key1 = 'valid';
        $key2 = 'invalid';

        $obj = new \stdClass();

        $this->collection->add($obj, $key1);

        $this->assertTrue($this->collection->keyExists($key1));
        $this->assertFalse($this->collection->keyExists($key2));
    }

    public function testLength()
    {
        $obj1 = new \stdClass();
        $obj2 = new \stdClass();

        $this
            ->collection
            ->add($obj1)
            ->add($obj2)
        ;

        $this->assertEquals($this->collection->length(), 2);
    }
}
