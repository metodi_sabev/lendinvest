<?php

namespace LendinvestTest\Service;

use Lendinvest\Entity\InvestmentEntity;
use Lendinvest\Entity\TrancheEntity;
use Lendinvest\Service\CalculateInterest;

class CalculateInterestTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var CalculateInterest
     */
    protected $calculateInterest;

    public function setUp()
    {
        $this->calculateInterest = new CalculateInterest();
    }

    public function testConstruction()
    {
        $this->assertInstanceOf(CalculateInterest::class, $this->calculateInterest);
    }

    // public function testInvestmentStatusFalse()
    // {
    //     $investment = new InvestmentEntity();

    //     $this->assertFalse(($this->calculateInterest)($investment));
    // }

    public function testInvestmentCalculator()
    {
        $earning = 54.84;
        $tranche = new TrancheEntity('Test', 10, 1000);

        $investment = new InvestmentEntity();
        $investment->setAmount(1000);
        $investment->setDate(new \DateTime('2017-01-15'));
        $investment->setTranche($tranche);

        $this->assertEquals($earning, ($this->calculateInterest)($investment));
    }
}
