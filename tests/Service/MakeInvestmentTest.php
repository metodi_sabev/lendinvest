<?php

namespace LendinvestTest\Service;

use Lendinvest\Constant;
use Lendinvest\Entity\InvestmentEntity;
use Lendinvest\Entity\InvestorEntity;
use Lendinvest\Entity\WalletEntity;
use Lendinvest\Exception\InvestmentException;
use Lendinvest\Service\CreateLoan;
use Lendinvest\Service\MakeInvestment;

class MakeInvestmentTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var MakeInvestment
     */
    protected $makeInvestment;

    public function setUp()
    {
        $this->makeInvestment = new MakeInvestment();
    }

    public function testConstrucion()
    {
        $this->assertInstanceOf(MakeInvestment::class, $this->makeInvestment);
    }

    public function testMissingTranche()
    {
        $this->expectException(InvestmentException::class);

        $loan = (new CreateLoan())();
        $trancheName = 'INVALID';

        $investor = $this->getMockBuilder(InvestorEntity::class)->disableOriginalConstructor()->getMock();

        ($this->makeInvestment)($investor, 500, $loan, $trancheName, new \DateTime('2015-10-04'));
    }

    public function testInvestmentAmountReached()
    {
        $this->expectException(InvestmentException::class);

        $investment = new InvestmentEntity();
        $investment->setAmount(1000);
        $investment->setDate(new \DateTime('2015-10-03'));

        $trancheName = 'A';
        $loan = (new CreateLoan())();
        $loan->getTranche($trancheName)->addInvestment($investment);

        $investor = $this->getMockBuilder(InvestorEntity::class)->disableOriginalConstructor()->getMock();

        $investor
            ->expects($this->at(0))
            ->method('getWalletAmount')
            ->willReturn(1000)
        ;

        ($this->makeInvestment)($investor, 500, $loan, $trancheName, new \DateTime('2015-10-04'));
    }

    public function testInvestmentAmountWillBeExceeded()
    {
        $this->expectException(InvestmentException::class);

        $investment = new InvestmentEntity();
        $investment->setAmount(500);
        $investment->setDate(new \DateTime('2015-10-03'));

        $trancheName = 'A';
        $loan = (new CreateLoan())();
        $loan->getTranche($trancheName)->addInvestment($investment);

        $investor = $this->getMockBuilder(InvestorEntity::class)->disableOriginalConstructor()->getMock();

        $investor
            ->expects($this->at(0))
            ->method('getWalletAmount')
            ->willReturn(1000)
        ;

        ($this->makeInvestment)($investor, 501, $loan, $trancheName, new \DateTime('2015-10-04'));
    }

    public function testLoanIsInactive()
    {
        $this->expectException(InvestmentException::class);

        $investor = $this->getMockBuilder(InvestorEntity::class)->disableOriginalConstructor()->getMock();

        $trancheName = 'A';
        $loan = (new CreateLoan())();

        $investor->expects($this->at(0))->method('getWalletAmount')->willReturn(1000);

        ($this->makeInvestment)($investor, 501, $loan, $trancheName, new \DateTime('2017-12-31'));
    }
}
