<?php

namespace LendinvestTest\Service;

use Lendinvest\Entity\LoanEntity;
use Lendinvest\Entity\TrancheEntity;
use Lendinvest\Exception\LoanException;
use Lendinvest\Service\CreateLoan;

class CreateLoanTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var CreateLoan
     */
    protected $createLoan;

    public function setUp()
    {
        $this->createLoan = new CreateLoan();
    }

    public function testConstruction()
    {
        $this->assertInstanceOf(CreateLoan::class, $this->createLoan);
    }

    public function testLoanTranches()
    {
        $this->expectException(LoanException::class);

        $this->assertInstanceOf(TrancheEntity::class, ($this->createLoan)()->getTranche('A'));
        $this->assertInstanceOf(TrancheEntity::class, ($this->createLoan)()->getTranche('B'));
        ($this->createLoan)()->getTranche('INVALID');
    }
}
