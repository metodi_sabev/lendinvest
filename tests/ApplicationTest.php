<?php

namespace LendinvestTest;

use Lendinvest\Application;

class ApplicationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Application
     */
    protected $application;

    public function setUp()
    {
        $this->application = new Application();
    }

    public function testConstruction()
    {
        $this->assertInstanceOf(Application::class, $this->application);
    }

    /**
     * @runInSeparateProcess
     */
    public function testIsNotCli()
    {
        $this->assertTrue($this->application->start());
    }
}
