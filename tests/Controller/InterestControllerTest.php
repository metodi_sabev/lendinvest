<?php

namespace LendinvestTest\Controller;

use Lendinvest\Controller\InterestController;

class InterestControllerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var InterestController
     */
    protected $interestController;

    public function setUp()
    {
        $this->interestController = new InterestController();
    }

    public function testContruction()
    {
        $this->assertInstanceOf(InterestController::class, $this->interestController);
    }

    public function testEmptyInvoke()
    {
        $this->assertFalse(($this->interestController)([], []));
    }

    public function testInvokeWithInvalidTranche()
    {
        $investor = [
            'name' => 'Investor 1',
            'wallet' => 1000,
        ];

        $investment = [
            'amount' => 1000,
            'date' => '2015-10-03',
            'trancheName' => 'INVALID',
            'investorName' => 'Investor 1',
        ];

        $this->assertTrue(($this->interestController)([$investor], [$investment]));
    }

    public function testInvoke()
    {
        $investor = [
            'name' => 'Investor 1',
            'wallet' => 1000,
            'investment' => [
                'amount' => 1000,
                'date' => '2015-10-03',
                'trancheName' => 'A',
            ],
        ];

        $investment = [
            'amount' => 1000,
            'date' => '2015-10-03',
            'trancheName' => 'A',
            'investorName' => 'Investor 1',
        ];

        $this->assertTrue(($this->interestController)([$investor], [$investment]));
    }
}
