<?php

namespace LendinvestTest\Entity;

use Lendinvest\Entity\LoanEntity;
use Lendinvest\Entity\TrancheEntity;
use Lendinvest\Exception\LoanException;

class LoanEntityTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @var LoanEntity
     */
    protected $loanEntity;

    public function setUp()
    {
        $this->startDate = new \DateTime('2015-01-01');
        $this->endDate = new \DateTime('2015-12-31');

        $this->loanEntity = new LoanEntity($this->startDate, $this->endDate);
    }

    public function testContruction()
    {
        $this->assertInstanceOf(LoanEntity::class, $this->loanEntity);
    }

    public function testGetStartDate()
    {
        $this->assertEquals($this->startDate, $this->loanEntity->getStartDate());
    }

    public function testGetEndDate()
    {
        $this->assertEquals($this->endDate, $this->loanEntity->getEndDate());
    }

    public function testRemoveTranche()
    {
        $this->expectException(LoanException::class);

        $trancheName = 'Test';
        $tranche = new TrancheEntity($trancheName, 1, 1000);

        $this->loanEntity->addTranche($tranche);

        $this->assertInstanceOf(TrancheEntity::class, $this->loanEntity->getTranche($trancheName));
        $this->loanEntity->removeTranche($trancheName);
        $this->loanEntity->getTranche($trancheName);
    }
}
