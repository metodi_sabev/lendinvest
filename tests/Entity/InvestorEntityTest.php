<?php

namespace LendinvestTest\Entity;

use Lendinvest\Entity\InvestmentEntity;
use Lendinvest\Entity\InvestorEntity;
use Lendinvest\Entity\WalletEntity;

class InvestorEntityTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var WalletEntity
     */
    protected $wallet;

    /**
     * @var InvestorEntity
     */
    protected $investorEntity;

    public function setUp()
    {
        $this->name = 'Test Investor';
        $this->amount = 5000;
        $this->wallet = new WalletEntity($this->amount);

        $this->investorEntity = new InvestorEntity($this->name, $this->wallet);
    }

    public function testContruction()
    {
        $this->assertInstanceOf(InvestorEntity::class, $this->investorEntity);
    }

    public function testInvestorName()
    {
        $name = 'Test name';

        $this->investorEntity->setName($name);

        $this->assertEquals($name, $this->investorEntity->getName());
    }

    public function testInvestorWallet()
    {
        $this->assertInstanceOf(WalletEntity::class, $this->investorEntity->getWallet());
        $this->assertEquals($this->amount, $this->investorEntity->getWallet()->getAmount());
    }

    public function testInvestorWithInvestment()
    {
        $investment = $this->getMockBuilder(InvestmentEntity::class)->disableOriginalConstructor()->getMock();

        $this->assertInstanceOf(InvestorEntity::class, $this->investorEntity->addInvestment($investment));
        $this->assertEquals(1, count($this->investorEntity->getInvestments()));
        $this->assertInstanceOf(InvestmentEntity::class, $this->investorEntity->getInvestments()[0]);
    }
}
