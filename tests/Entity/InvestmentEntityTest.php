<?php

namespace LendinvestTest\Entity;

use Lendinvest\Entity\InvestmentEntity;
use Lendinvest\Entity\InvestorEntity;

class InvestmentEntityTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var InvestmentEntity
     */
    protected $investmentEntity;

    public function setUp()
    {
        $this->investmentEntity = new InvestmentEntity();
    }

    public function testConstruction()
    {
        $this->assertInstanceOf(InvestmentEntity::class, $this->investmentEntity);
    }

    public function testInvestmentWithInvestor()
    {
        $investor = $this->getMockBuilder(InvestorEntity::class)->disableOriginalConstructor()->getMock();

        $this->investmentEntity->setInvestor($investor);

        $this->assertInstanceOf(InvestorEntity::class, $this->investmentEntity->getInvestor());
    }
}
