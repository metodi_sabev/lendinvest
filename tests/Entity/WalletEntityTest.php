<?php

namespace LendinvestTest\Entity;

use Lendinvest\Entity\WalletEntity;

class WalletEntityTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var WalletEntity
     */
    protected $walletEntity;

    public function setUp()
    {
        $amount = 1000;

        $this->walletEntity = new WalletEntity($amount);
    }

    public function testContruction()
    {
        $this->assertInstanceOf(WalletEntity::class, $this->walletEntity);
    }
}
