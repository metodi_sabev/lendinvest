<?php

namespace LendinvestTest\Entity;

use Lendinvest\Entity\InvestmentEntity;
use Lendinvest\Entity\TrancheEntity;

class TrancheEntityTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var TrancheEntity
     */
    protected $trancheEntity;

    public function setUp()
    {
        $name   = 'test';
        $rate   = 10;
        $amount = 1000;

        $this->trancheEntity = new TrancheEntity($name, $rate, $amount);
    }

    public function testConstruction()
    {
        $this->assertInstanceOf(TrancheEntity::class, $this->trancheEntity);
    }

    public function testInvestments()
    {
        $investment = $this->getMockBuilder(InvestmentEntity::class)->disableOriginalConstructor()->getMock();

        $this->trancheEntity->addInvestment($investment);

        $this->assertEquals(1, count($this->trancheEntity->getInvestments()));
        $this->assertEquals($investment, $this->trancheEntity->getInvestments()[0]);
    }

    public function testGetTotalInvestmentsAmount()
    {
        $investment = new InvestmentEntity();
        $investment->setAmount(1500);
        $investment->setDate(new \DateTime('2017-01-10'));

        $investment2 = new InvestmentEntity();
        $investment2->setAmount(750);
        $investment2->setDate(new \DateTime('2017-01-15'));

        $startDate = new \DateTime('2017-01-01');
        $endDate = new \DateTime('2017-01-31');

        $this->trancheEntity->addInvestment($investment);
        $this->trancheEntity->addInvestment($investment2);

        $this->assertEquals(2250, $this->trancheEntity->getTotalInvestmentAmount($startDate, $endDate));
    }
}
