<?php

namespace Lendinvest;

/**
 * Class Collection.
 * @package Lendinvest
 */
class Collection
{
    /**
     * @var array
     */
    private $items = [];

    /**
     * Add an item to the collection.
     *
     * @param mixed $obj
     * @param mixed $key
     *
     * @return Collection
     *
     * @throws \Exception
     */
    public function add($obj, $key = null): self
    {
        if ($key === null) {
            $this->items[] = $obj;

            return $this;
        }

        if (isset($this->items[$key])) {
            throw new \Exception(sprintf('The %s key is already in use.', $key));
        }

        $this->items[$key] = $obj;

        return $this;
    }

    /**
     * Delete an item from the collection.
     *
     * @param  mixed $key
     *
     * @return Collection
     *
     * @throws \Exception
     */
    public function delete($key): self
    {
        if (!isset($this->items[$key])) {
            throw new \Exception(sprintf('Invalid key %s', $key));
        }

        unset($this->items[$key]);

        return $this;
    }

    /**
     * Get an item from the collection.
     *
     * @param  mixed $key
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function get($key)
    {
        if (!isset($this->items[$key])) {
            throw new \Exception(sprintf('Invalid key %s', $key));
        }

        return $this->items[$key];
    }

    /**
     * Check if a key already exists.
     *
     * @param  mixed $key
     *
     * @return bool
     */
    public function keyExists($key): bool
    {
        return isset($this->items[$key]);
    }

    /**
     * Get the length of the items in the array.
     *
     * @return int
     */
    public function length(): int
    {
        return count($this->items);
    }

    /**
     * Get all items as array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->items;
    }
}
