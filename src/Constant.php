<?php

namespace Lendinvest;

/**
 * Class Constant
 */
class Constant
{
    /** Investment error messages */
    const ERROR_AMOUNT_REACHED      = 'Investment cannot be placed. Total investment amount already reached.';
    const ERROR_AMOUNT_WILL_EXCEED  = 'Investment cannot be placed. The investment amount is exceeding the maximum allowed amount.';
    const ERROR_INSUFFICIENT_AMOUNT = 'Investment cannot be placed. The amount in the wallet is insufficient.';
    const ERROR_LOAN_EXPIRED        = 'Investment cannot be placed. The loan period has expired.';
    const ERROR_INVALID_TRANCHE     = 'Investment cannot be placed. Invalid tranche given.';
}
