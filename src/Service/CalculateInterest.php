<?php

namespace Lendinvest\Service;

use Lendinvest\Entity\InvestmentEntity;

/**
 * Class CalculateInterest
 * @package Lendinvest\Service
 */
class CalculateInterest
{
    /**
     * @param  InvestmentEntity $investment
     *
     * @return string|bool
     */
    public function __invoke(InvestmentEntity $investment)
    {
        $amount         = $investment->getAmount();
        $investmentDate = $investment->getDate();
        $rate           = $investment->getTranche()->getInterestRate() / 100;

        $firstDayOfMonth    = (clone $investmentDate)->modify('first day of this month');
        $lastDayOfMonth     = (clone $investmentDate)->modify('last day of this month');
        $unpaidDays         = $investmentDate->diff($firstDayOfMonth)->format("%d");

        $monthlyInterest    = $amount * $rate;
        $dailyInterest      = $monthlyInterest / $lastDayOfMonth->format("d");

        return number_format($monthlyInterest - ($dailyInterest * $unpaidDays), 2, '.', '');
    }
}
