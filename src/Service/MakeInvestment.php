<?php

namespace Lendinvest\Service;

use DateTime;
use Lendinvest\Constant;
use Lendinvest\Entity\InvestmentEntity;
use Lendinvest\Entity\InvestorEntity;
use Lendinvest\Entity\LoanEntity;
use Lendinvest\Entity\TrancheEntity;
use Lendinvest\Exception\InvestmentException;

/**
 * Class MakeInvestment
 * @package Lendinvest\Service
 */
class MakeInvestment
{
    /**
     * Make an investment.
     *
     * @param InvestorEntity $investor
     * @param float          $amount
     * @param LoanEntity     $tranche
     * @param string         $trancheName
     * @param DateTime       $date
     *
     * @return InvestmentEntity
     *
     * @throws InvestmentException
     */
    public function __invoke(InvestorEntity $investor, float $amount, LoanEntity $loan, string $trancheName, DateTime $date): InvestmentEntity
    {
        if ($investor->getWalletAmount() < $amount) {
            throw new InvestmentException(Constant::ERROR_INSUFFICIENT_AMOUNT);
        } elseif (!$loan->isActive($date)) {
            throw new InvestmentException(Constant::ERROR_LOAN_EXPIRED);
        }

        $tranche = $loan->getTranche($trancheName);

        list($startDate, $endDate) = $this->getStartAndEndDates($date);
        $investedAmount = $tranche->getTotalInvestmentAmount($startDate, $endDate);
        $totalAmount = $tranche->getAmount();

        if ($investedAmount == $totalAmount) {
            throw new InvestmentException(Constant::ERROR_AMOUNT_REACHED);
        } elseif (($amount + $investedAmount) > $totalAmount) {
            throw new InvestmentException(Constant::ERROR_AMOUNT_WILL_EXCEED);
        }

        return $this->createInvestment($investor, $amount, $date, $tranche);
    }

    /**
     * Create investment.
     *
     * @param  InvestorEntity $investor
     * @param  float          $amount
     * @param  DateTime       $date
     * @param  TrancheEntity  $tranche
     *
     * @return InvestmentEntity
     */
    private function createInvestment(InvestorEntity $investor, float $amount, DateTime $date, TrancheEntity $tranche): InvestmentEntity
    {
        $wallet = $investor->getWallet();
        $wallet->setAmount($wallet->getAmount() - $amount);

        $investment = new InvestmentEntity();
        $investment->setAmount($amount);
        $investment->setDate($date);
        $investment->setInvestor($investor);
        $investment->setTranche($tranche);

        $tranche->addInvestment($investment);

        return $investment;
    }

    /**
     * Get the first and the last days of the month.
     *
     * @param  DateTime $date
     *
     * @return array
     */
    private function getStartAndEndDates(DateTime $date): array
    {
        return [
            (clone $date)->modify('first day of this month'),
            (clone $date)->modify('last day of this month'),
        ];
    }
}
