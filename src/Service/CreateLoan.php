<?php

namespace Lendinvest\Service;

use Lendinvest\Entity\LoanEntity;
use Lendinvest\Entity\TrancheEntity;

/**
 * Class CreateLoan
 * @package Lendinvest\Service
 */
class CreateLoan
{
    const TRANCHE_A = [
        'name'      => 'A',
        'rate'      => 3,
        'amount'    => 1000,
    ];

    const TRANCHE_B = [
        'name'      => 'B',
        'rate'      => 6,
        'amount'    => 1000,
    ];
    
    const LOAN = [
        'start' => '2015-10-01',
        'end'   => '2015-11-15',
    ];

    /**
     * Create a loan with its corresponding tranches.
     *
     * @return LoanEntity
     */
    public function __invoke(): LoanEntity
    {
        $trancheA = new TrancheEntity(
            self::TRANCHE_A['name'],
            self::TRANCHE_A['rate'],
            self::TRANCHE_A['amount']
        );
        $trancheB = new TrancheEntity(
            self::TRANCHE_B['name'],
            self::TRANCHE_B['rate'],
            self::TRANCHE_B['amount']
        );

        $loan = new LoanEntity(
            new \DateTime(self::LOAN['start']),
            new \DateTime(self::LOAN['end'])
        );
        $loan->addTranche($trancheA);
        $loan->addTranche($trancheB);

        return $loan;
    }
}
