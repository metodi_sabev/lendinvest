<?php

namespace Lendinvest\Entity;

/**
 * Class WalletEntity.
 * @package Lendinvest\Entity
 */
class WalletEntity
{
    /**
     * @var float
     */
    protected $amount;

    /**
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @param float $amount
     *
     * @return WalletEntity
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}
