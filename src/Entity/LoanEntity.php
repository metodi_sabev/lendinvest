<?php

namespace Lendinvest\Entity;

use DateTime;
use Lendinvest\Collection;
use Lendinvest\Constant;
use Lendinvest\Entity\TrancheEntity;
use Lendinvest\Exception\LoanException;

/**
 * Class LoanEntity
 * @package Lendinvest\Entity
 */
class LoanEntity
{
    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;

    /**
     * @var Collection
     */
    private $tranches;

    /**
     * @param DateTime $startDate
     * @param DateTime $endDate
     */
    public function __construct(DateTime $startDate, DateTime $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;

        $this->tranches = new Collection();
    }

    /**
     * Get start date.
     *
     * @return DateTime
     */
    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * Get end date.
     *
     * @return DateTime
     */
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @param TrancheEntity $tranche
     *
     * @return self
     */
    public function addTranche(TrancheEntity $tranche): self
    {
        $this->tranches->add($tranche, $tranche->getName());

        return $this;
    }

    /**
     * @param  string $trancheName
     *
     * @return self
     */
    public function removeTranche(string $trancheName): self
    {
        if ($this->tranches->keyExists($trancheName)) {
            $this->tranches->delete($trancheName);
        }

        return $this;
    }

    /**
     * @param  string $trancheName
     *
     * @return TrancheEntity
     *
     * @throws LoanException
     */
    public function getTranche(string $trancheName): TrancheEntity
    {
        if ($this->tranches->keyExists($trancheName)) {
            return $this->tranches->get($trancheName);
        }

        throw new LoanException(sprintf(Constant::ERROR_INVALID_TRANCHE, $trancheName));
    }

    /**
     * Check if the loan is still active.
     *
     * @param  DateTime $investmentDate
     *
     * @return boolean
     */
    public function isActive(DateTime $investmentDate): bool
    {
        return ($this->getStartDate() <= $investmentDate && $this->getEndDate() >= $investmentDate);
    }
}
