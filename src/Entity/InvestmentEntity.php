<?php

namespace Lendinvest\Entity;

use Lendinvest\Entity\InvestorEntity;
use Lendinvest\Entity\TrancheEntity;

/**
 * Class InvestmentEntity
 * @package Lendinvest\Entity
 */
class InvestmentEntity
{
    /**
     * @var float
     */
    protected $amount;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var TrancheEntity
     */
    protected $tranche;
    
    /**
     * @var InvestorEntity
     */
    protected $investor;

    /**
     * @param float $amount
     *
     * @return self
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param \DateTime $date
     *
     * @return self
     */
    public function setDate(\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param TrancheEntity $tranche
     *
     * @return self
     */
    public function setTranche(TrancheEntity $tranche): self
    {
        $this->tranche = $tranche;

        return $this;
    }

    /**
     * @return TrancheEntity
     */
    public function getTranche(): TrancheEntity
    {
        return $this->tranche;
    }

    /**
     * @param InvestorEntity $investor
     *
     * @return self
     */
    public function setInvestor(InvestorEntity $investor): self
    {
        $this->investor = $investor;

        return $this;
    }

    /**
     * @return InvestorEntity
     */
    public function getInvestor(): InvestorEntity
    {
        return $this->investor;
    }
}
