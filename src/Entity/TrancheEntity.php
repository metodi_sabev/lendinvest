<?php

namespace Lendinvest\Entity;

use Lendinvest\Collection;
use Lendinvest\Entity\InvestmentEntity;
use Lendinvest\Entity\LoanEntity;

/**
 * Class TrancheEntity
 * @package Lendinvest\Entity
 */
class TrancheEntity
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $interestRate;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var Collection
     */
    protected $investments;

    /**
     * @param string $name
     * @param int    $interestRate
     * @param float  $amount
     */
    public function __construct(string $name, int $interestRate, float $amount)
    {
        $this->name = $name;
        $this->interestRate = $interestRate;
        $this->amount = $amount;

        $this->investments = new Collection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getInterestRate(): int
    {
        return $this->interestRate;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param InvestmentEntity $investment
     *
     * @return self
     */
    public function addInvestment(InvestmentEntity $investment): self
    {
        $this->investments->add($investment);

        return $this;
    }

    /**
     * @return array
     */
    public function getInvestments(): array
    {
        return $this->investments->toArray();
    }

    /**
     * @param  \DateTime $startDate
     * @param  \DateTime $endDate
     *
     * @return float
     */
    public function getTotalInvestmentAmount(\DateTime $startDate, \DateTime $endDate): float
    {
        $amount = 0;

        foreach ($this->investments->toArray() as $investment) {
            if ($investment->getDate() >= $startDate && $investment->getDate() <= $endDate) {
                $amount += $investment->getAmount();
            }
        }

        return $amount;
    }
}
