<?php

namespace Lendinvest\Entity;

use Lendinvest\Collection;
use Lendinvest\Entity\InvestmentEntity;
use Lendinvest\Entity\WalletEntity;

/**
 * Class InvestorEntity
 */
class InvestorEntity
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var WalletEntity
     */
    protected $wallet;

    /**
     * @var Collection
     */
    protected $investments;

    /**
     * @param string        $name
     * @param WalletEntity  $wallet
     */
    public function __construct(string $name, WalletEntity $wallet)
    {
        $this->name = $name;
        $this->wallet = $wallet;

        $this->investments = new Collection();
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return WalletEntity
     */
    public function getWallet(): WalletEntity
    {
        return $this->wallet;
    }

    /**
     * @param InvestmentEntity $investment
     *
     * @return self
     */
    public function addInvestment(InvestmentEntity $investment): self
    {
        $this->investments->add($investment);

        return $this;
    }

    /**
     * @return array
     */
    public function getInvestments(): array
    {
        return $this->investments->toArray();
    }

    /**
     * @return float
     */
    public function getWalletAmount(): float
    {
        return $this->getWallet()->getAmount();
    }
}
