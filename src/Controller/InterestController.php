<?php

namespace Lendinvest\Controller;

use Lendinvest\Entity\InvestorEntity;
use Lendinvest\Entity\WalletEntity;
use Lendinvest\Exception\InvestmentException;
use Lendinvest\Exception\LoanException;
use Lendinvest\Service\CalculateInterest;
use Lendinvest\Service\CreateLoan;
use Lendinvest\Service\MakeInvestment;

/**
 * Class InterestController
 * @package Lendinvest\Controller
 */
class InterestController
{
    /**
     * @param  array  $investorsData
     * @param  array  $investmentsData
     *
     * @return bool
     */
    public function __invoke(array $investorsData, array $investmentsData): bool
    {
        if (empty($investorsData) || empty($investmentsData)) {
            return false;
        }

        /** @var LoanEntity */
        $loan = (new CreateLoan())();
        $makeInvestmentService = new MakeInvestment();
        $investors = $this->initInvestors($investorsData);

        $investments = [];
        foreach ($investmentsData as $key => $data) {
            try {
                $investments[] = $makeInvestmentService(
                    $investors[$data['investorName']],
                    $data['amount'],
                    $loan,
                    $data['trancheName'],
                    new \DateTime($data['date'])
                );
            } catch (InvestmentException $e) {
                // @ToDo Log error messages here
            } catch (LoanException $e) {
                // @ToDo Log error messages here
            }
        }

        /** Calculate earnings and print out the results */
        $interestCalculator = new CalculateInterest();
        foreach ($investments as $investment) {
            $investorName   = $investment->getInvestor()->getName();
            $earning        = $interestCalculator($investment);

            print(sprintf("%s : %0.2f", $investorName, $earning) . PHP_EOL);
        }

        return true;
    }

    /**
     * Initialize the investors objects with the given input data.
     *
     * @param  array  $investorsData
     *
     * @return array
     */
    private function initInvestors(array $investorsData): array
    {
        $investors = [];

        foreach ($investorsData as $key => $data) {
            $investor = new InvestorEntity($data['name'], new WalletEntity($data['wallet']));

            $investors[$investor->getName()] = $investor;
        }

        return $investors;
    }
}
