<?php

namespace Lendinvest;

use Lendinvest\Controller\InterestController;

/**
 * Class Application.
 * @package Lendinvest
 */
class Application
{
    const INVESTORS_INPUT_FILE_NAME = 'testInputDataInvestors.json';
    const INVESTMENTS_INPUT_FILE_NAME = 'testInputDataInvestments.json';

    /**
     * Launch the application.
     *
     * @return bool
     */
    public function start(): bool
    {
        if (php_sapi_name() !== 'cli') {
            return false;
        }

        /** Simple routing can be added here */
        return (new InterestController())($this->getInvestors(), $this->getInvestments());
    }

    /**
     * Read the investors input data from file.
     *
     * @return array
     */
    private function getInvestors(): array
    {
        return $this->getInputData(self::INVESTORS_INPUT_FILE_NAME);
    }

    /**
     * Get the investments input data from file.
     *
     * @return array
     */
    private function getInvestments(): array
    {
        return $this->getInputData(self::INVESTMENTS_INPUT_FILE_NAME);
    }

    /**
     * Get input data. This app version supports only hardcoded as
     * constants data.
     *
     * @return array
     */
    private function getInputData(string $fileName): array
    {
        $file = new \SplFileObject(__DIR__ . '/../' . $fileName, 'r');
        $data = $file->fread($file->getSize());

        return json_decode($data, true);
    }
}
